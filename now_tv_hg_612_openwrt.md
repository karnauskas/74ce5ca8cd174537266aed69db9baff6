I am hoping to save a few hours for people like me who have a modem + router setup and switched to NOW TV ISP.

**Extract Client ID and Vendor Class ID from DHCP handshake**

- Connect to the NOW TV router and select "WANOE only" from advanced > router mode
- With a computer connected to LAN 2, using Wireshark, collect DHCP Client ID and Vendor Class ID (options 60 and 61) (1)
- Convert those two values to hexadecimal

**Configuration for HG 612**

_ATM configuration:_

- VPI/VCI: 0/38
- DSL link type: EoA
- Encapsulation mode: VCMUX
- Service type: UBR Without PCR 

_WAN configuration_
- WAN connection: Enabled
- Service list: INTERNET
- Connection Mode: Bridge
- Bridge type: IP_Bridged
- DHCP transparent transmission: ENABLE
- WAN 802.1q: Enable
- VLAN ID: 101
- WAN 802.1p: Enable
- Value 2

**Configuration for OpenWRT WAN **
- Protocol: DHCP client
- Advanced Settings: 
- Client ID: the HEX string resulted from step 1
- Vendor Class: the HEX string resulted from step 1


(1) reading on various forms, for Client ID, it seems this is unnecessary, and it will work with anything similar to 3c1ec9286880@now|gPPjtOxg converted to HEX.